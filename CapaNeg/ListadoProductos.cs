﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNeg
{
    public class ListadoProductos
    {
        public int id { get; set; }
        public string nombre_producto { get; set; }
        public int? cantidad { get; set; }
        public int? valor { get; set; }
        public System.DateTime? fecha_vencimiento { get; set; }
        public int? fk_usuarios { get; set; }
    }
}

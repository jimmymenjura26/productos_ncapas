﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNeg
{
    public class Productos
    {
        public static List<ListadoProductos> Obtener()
        {
            using (CapaDatos.inventarios_alpinaEntities datos = new CapaDatos.inventarios_alpinaEntities())
            {
                return  (from d in  datos.productos
                        join u in datos.usuarios on d.fk_usuarios equals u.identificacion
                       select new ListadoProductos() {
                           id =  d.id,
                           nombre_producto = d.nombre_producto,
                           cantidad = d.cantidad,
                           valor = d.valor,
                           fecha_vencimiento = d.fecha_vencimiento,
                           fk_usuarios =d.fk_usuarios
                       }).ToList();
            }
        }

    }
}

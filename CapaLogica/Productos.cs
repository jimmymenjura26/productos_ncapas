﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaLogica
{
    class Productos
    {
        public static List<CapaDatos.productos> Obtener()
        {
            using (CapaDatos.inventarios_alpinaEntities1 datos = new CapaDatos.inventarios_alpinaEntities1()) {
                return datos.productos.ToList();
            }
        }
    }
}
